

public enum Ingredient
{
    PBT("peanutbutter"),
    BRD("bread"),
    JAM("jam");
    
    private String description;
    
    private Ingredient(String description){
        this.description = description;
    }
    
    public String toString(){
        return description;
    }
}
