
/**
 * Write a description of class LetsMakeSandwitches here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class LetsMakeSandwitches
{
    
public static void main(String[] args){
    Table table = new Table();
    Thread chef1, chef2, chef3, agent;
    
   
    chef1 = new Thread( new Chef(Ingredient.PBT, table),"chef1");
    chef2 = new Thread( new Chef(Ingredient.BRD, table),"chef2");
    chef3 = new Thread( new Chef(Ingredient.JAM, table),"chef3");
    
    agent = new Thread( new Agent(table),"agent");

    
    chef2.start();
    chef3.start();
    chef1.start();
    
    agent.start();
    

    
}
}
