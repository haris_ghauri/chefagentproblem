import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class Agent implements Runnable
{
    private Table table;
    private ArrayList<Ingredient> myIngredients = new ArrayList<Ingredient>();
    
    public Agent(Table table){
        this.table = table;
        myIngredients.add(Ingredient.PBT);
        myIngredients.add(Ingredient.BRD);
        myIngredients.add(Ingredient.JAM);
    }
    
    
    
    public ArrayList<Ingredient> pickRandom(ArrayList<Ingredient> lst, int n) {
    ArrayList<Ingredient> copy = new ArrayList<Ingredient>(lst);
    Collections.shuffle(copy);
    return new ArrayList<Ingredient>(copy.subList(0,n));
    }
    
    
    
    public void run(){
        
        for (int i = 0; i < 20 ; i++){
        ArrayList<Ingredient> pickedIngredients = pickRandom(myIngredients, 2);
        Ingredient ingredient1 = pickedIngredients.get(0);
        Ingredient ingredient2 = pickedIngredients.get(1);
        table.placeIngredientsOntheTable(ingredient1, ingredient2);
    }
   
        
    }
    
 
}
