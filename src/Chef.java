

public class Chef implements Runnable
{
    private Table table;
    private Ingredient myIngredient;
    
    public Chef(Ingredient ingredient, Table table){
        this.myIngredient = ingredient;
        this.table = table;
    }
    
     public void run()
    {
    	System.out.println("Thread " + Thread.currentThread() +"'s ingredient is "+myIngredient);
        while(true){
        table.makeSandwitch(myIngredient);
    }
    }
    
   

}
