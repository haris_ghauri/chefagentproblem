import java.util.ArrayList;
public class Table
{
    public static final int SIZE = 2; // the number of ingredients that can be placed on the table
    private Ingredient[] currentIngredients = new Ingredient[SIZE]; // ingredients currently placed on the table 
   // private int inIndex = 0, outIndex = 0, count = 0;
    
    private boolean hasRoom = true; //if true, there is space available for ingredient to be placed on the table
                                    //otherwise table has all the three ingredients to make a sendwitch  
    private int SandwitchCount = 0;
    
    public synchronized void placeIngredientsOntheTable(Ingredient ingredient1, Ingredient ingredient2){
        while (!hasRoom){
             try { 
                 System.out.println("Agent is waiting...");
                wait();
            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }
   
        
        currentIngredients[0] = ingredient1;
        currentIngredients[1] = ingredient2;
        System.out.println("Agent has placed the ingredients " +ingredient1+ " and "+ingredient2+" on the table");
        hasRoom = false;
            
        notifyAll();
    }
    

    
    public synchronized boolean haveAllNeccessaryIngredients(Ingredient ingredient){
       // System.out.println("reached here: coming from chef("+ingredient+")");
        if( (ingredient == currentIngredients[0]) || (ingredient == currentIngredients[1]  ) )
        return false;
        
        
        return true;
    }
    
    public synchronized void makeSandwitch(Ingredient ingredient){
        while(hasRoom || !haveAllNeccessaryIngredients(ingredient)){
            try{
                System.out.println("Chef("+ingredient+") is now going to wait..");
                wait();   
               
        }catch (InterruptedException e) {
                System.err.println(e);
            }
        
    }
    
    
    System.out.println("Chef("+ingredient+") has all the resources to make sandwitch.... So he is now making the sandwitch");
    SandwitchCount++;
    System.out.println(SandwitchCount+ " sandwitches made");
    currentIngredients[0] = null;
    currentIngredients[1] = null;
    hasRoom = true;
    notifyAll();
    
}


        
   

}
